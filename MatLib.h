#pragma once
#include<time.h>
#include<stdlib.h>
#include<iostream>
#include<exception>
#include<math.h>

using namespace std;

class DIM_EXP : public exception
{
	virtual const char* what() const throw()
	{
		return "Dimension Mismatch Exception";
	}
} DIMENTION_MISMATCH_EXCEPTION;

class Matrix
{
public:
	Matrix(int, int);
	Matrix(int, int, float);
	void sub_mat(int, int, int, int, Matrix*);
	void place(int, int, int, int, Matrix&);
	void make_square();
	void minimize();
	float row_sum(int);
	float col_sum(int);
	~Matrix();
	int rows;
	int cols;
	float** data;
	Matrix* operator*(Matrix&);
	void operator=(Matrix*);
	bool operator==(Matrix&);
	Matrix* operator+(Matrix*);
	Matrix* strassen(Matrix*);
	void show();

private:
	void _fil_rand();
	void _init_data();
	void _init_data(float);
};

Matrix::Matrix(int rows, int cols)
{
	this->rows = rows;
	this->cols = cols;

	_init_data();

	_fil_rand();
}

Matrix::Matrix(int rows, int cols, float init_val)
{
	this->rows = rows;
	this->cols = cols;

	_init_data(init_val);
}

void Matrix::sub_mat(int rs, int re, int cs, int ce, Matrix* o) 
{
	for (int i = 0; rs < re; rs++, i++)
	{
		int _cs = cs;
		for (int j = 0; _cs < ce; _cs++, j++)
		{
			data[i][j] = o->data[rs][_cs];
		}
	}
}

void Matrix::make_square()
{
	int max = rows;

	if (cols > rows)
		max = cols;

	max = (int)pow(2, ceil(log<int>(max)));

	float** temp = new float*[max];

	for (int i = 0; i < max; i++)
	{
		temp[i] = new float[max];
		
		for (int j = 0; j < max; j++)
		{
			temp[i][j] = 0;
		}
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			temp[i][j] = data[i][j];
		}
	}

	for (int i = 0; i < rows; i++)
	{
		delete [] data[i];
	}

	delete data;

	data = temp;
	temp = NULL;

	rows = max;
	cols = max;
}

float Matrix::row_sum(int row_no)
{
	float total = 0;
	
	for (int i = 0; i < cols; i++)
	{
		total += data[row_no][i];
	}

	return total;
}

float Matrix::col_sum(int col_no)
{
	float total = 0;

	for (int i = 0; i < rows; i++)
	{
		total += data[i][col_no];
	}

	return total;
}

void Matrix::minimize()
{
	int _rows = 0;
	int _cols = 0;

	for (int i = 0; i < rows; i++)
	{
		if (row_sum(i) == 0)
			break;
		_rows++;
	}

	for (int i = 0; i < cols; i++)
	{
		if (col_sum(i) == 0)
			break;
		_cols++;
	}

	float** temp = new float*[_rows];
	
	for (int i = 0; i < _rows; i++)
	{
		temp[i] = new float[_cols];
	}

	for (int i = 0; i < _rows; i++)
	{
		for (int j = 0; j < _cols; j++)
		{
			temp[i][j] = data[i][j];
		}
	}

	for (int i = 0; i < rows; i++)
	{
		delete [] data[i];
	}

	delete data;

	data = temp;
	temp = NULL;

	rows = _rows;
	cols = _cols;
}

void Matrix::show()
{
	for (int i = 0; i < rows; i++)
	{
		cout << endl;
		for (int j = 0; j < cols; j++)
		{
			cout << data[i][j] << "\t";
		}
	}
}

void Matrix::place(int rs, int re, int cs, int ce, Matrix& o)
{
	for (int i = 0; rs < re; rs++, i++)
	{
		int _cs = cs;
		for (int j = 0; _cs < ce; _cs++, j++)
		{
			data[rs][_cs] = o.data[i][j];
		}
	}
}

void Matrix::_init_data() 
{
	data = new float*[rows];
	for (int i = 0; i < rows; i++)
	{
		data[i] = new float[cols];
	}
}

void Matrix::operator=(Matrix* o)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			data[i][j] = o->data[i][j];
		}
	}
}

bool Matrix::operator==(Matrix& o)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			if (data[i][j] != o.data[i][j])
				return false;
		}
	}
	return true;
}

Matrix* Matrix::operator+(Matrix* o)
{
	if (rows != o->rows || cols != o->cols)
		throw DIMENTION_MISMATCH_EXCEPTION;

	Matrix* r = new Matrix(rows, cols);
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			r->data[i][j] = data[i][j] + o->data[i][j];
		}
	}
	return r;
}

void Matrix::_init_data(float init_val)
{
	data = new float*[rows];
	for (int i = 0; i < rows; i++)
	{
		data[i] = new float[cols];
		for (int j = 0; j < cols; j++)
		{
			data[i][j] = init_val;
		}
	}
}

Matrix* Matrix::operator*(Matrix& o)
{
	if (cols != o.rows)
		throw DIMENTION_MISMATCH_EXCEPTION;

	Matrix *r = new Matrix(rows, o.cols);

	for (int i = 0; i < r->rows; i++)
	{
		for (int j = 0; j < r->cols; j++)
		{
			r->data[i][j] = 0;
			//Sigma 0 to cols (DATAik * ODATAkj)
			for (int k = 0; k < cols; k++)
			{
				r->data[i][j] += data[i][k] * o.data[k][j];
			}
		}
	}

	return r;
}

Matrix* Matrix::strassen(Matrix* o)
{


	if (cols != o->rows)
		throw DIMENTION_MISMATCH_EXCEPTION;

	this->make_square();
	o->make_square();

	if (rows == 2)
	{
		return (*this * *o);
	}
	else 
	{
		int div_size = rows / 2;
		int end = rows;
		
		Matrix* r = new Matrix(rows, cols, 0);
		Matrix A11(div_size, div_size, 0);
		A11.sub_mat(0, div_size, 0, div_size, this);
		Matrix A12(div_size, div_size, 0);
		A12.sub_mat(0, div_size, div_size, end, this);
		Matrix A21(div_size, div_size, 0);
		A21.sub_mat(div_size, end, 0, div_size, this);
		Matrix A22(div_size, div_size, 0);
		A22.sub_mat(div_size, end, div_size, end, this);

		Matrix* B11 = new Matrix(div_size, div_size, 0);
		B11->sub_mat(0, div_size, 0, div_size, o);
		Matrix* B12 = new Matrix(div_size, div_size, 0);
		B12->sub_mat(0, div_size, div_size, end, o);
		Matrix* B21 = new Matrix(div_size, div_size, 0);
		B21->sub_mat(div_size, end, 0, div_size, o);
		Matrix* B22 = new Matrix(div_size, div_size, 0);
		B22->sub_mat(div_size, end, div_size, end, o);

		r->place(0, div_size, 0, div_size, *(*(A11.strassen(B11)) + (A12.strassen(B21))));
		r->place(0, div_size, div_size, end, *(*(A11.strassen(B12)) + (A12.strassen(B22))));
		r->place(div_size, end, 0, div_size, *(*(A21.strassen(B11)) + (A22.strassen(B21))));
		r->place(div_size, end, div_size, end, *(*(A21.strassen(B12)) + (A22.strassen(B22))));

		this->minimize();
		o->minimize();

		r->minimize();

		return r;
	}
}

Matrix::~Matrix()
{
	for (int i = 0; i < rows; i++)
	{
		delete[] data[i];
	}

	delete data;
}

void Matrix::_fil_rand()
{
	srand(time(NULL));
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			data[i][j] = rand() % 100;
		}
	}
}