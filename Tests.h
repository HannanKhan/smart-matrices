#pragma once

#include "MatLib.h"
#include <memory>

using namespace tr1;

bool test_create() 
{
	bool result = true;
	cout << endl << "Creating Matrix of size 2X2";
	Matrix test(2, 2);
	cout << endl << "Created Matrix's rows = " << test.rows << "\tMatrix's cols = " << test.cols;
	test.show();
	result &= (test.rows == 2 && test.cols == 2);

	cout << endl << "Creating Matrix of size 4X5";
	Matrix test2(4, 5);
	cout << endl << "Created Matrix's rows = " << test2.rows << "\tMatrix's cols = " << test2.cols;
	test2.show();
	result &= (test2.rows == 4 && test2.cols == 5);
	cout << endl << endl;

	return result;
}

bool test_init()
{
	bool result = true;
	cout << endl << "Creating Matrix of size 2X2 filled with 0";
	Matrix test(2, 2, 0);
	cout << endl << "Created Matrix's rows = " << test.rows << "\tMatrix's cols = " << test.cols;
	test.show();
	
	for (int i = 0; i < test.rows; i++)
	{
		for (int j = 0; j < test.cols; j++)
		{
			result &= (test.data[i][j] == 0);
		}
	}

	cout << endl << "Creating Matrix of size 4X5 filled with 2";
	Matrix test2(4, 5, 2);
	cout << endl << "Created Matrix's rows = " << test2.rows << "\tMatrix's cols = " << test2.cols;
	test2.show();
	
	for (int i = 0; i < test2.rows; i++)
	{
		for (int j = 0; j < test2.cols; j++)
		{
			result &= (test2.data[i][j] == 2);
		}
	}
	cout << endl << endl;

	return result;
}

bool test_sub_mat()
{
	bool result = true;
	cout << endl << "Creating Matrix of size 2X2";
	Matrix test(2, 2);
	cout << endl << "Created Matrix A's rows = " << test.rows << "\tMatrix A's cols = " << test.cols;
	test.show();

	cout << endl << "Creating Matrix of size 4X5";
	Matrix test2(4, 5);
	cout << endl << "Created Matrix B's rows = " << test2.rows << "\tMatrix B's cols = " << test2.cols;
	test2.show();
	
	cout << endl << "setting B[2:4, 3:5] = A";
	test.sub_mat(2, 4, 3, 5, &test2);
	cout << endl << "Matrix A after operation";
	test.show();

	for (int i = 0, r = 2; r < 4; r++, i++)
	{
		for (int j = 0, c = 3; c < 5; c++, j++)
		{
			result &= (test.data[i][j] == test2.data[r][c]);
		}
	}

	cout << endl << endl;

	return result;
}

bool test_strassen()
{
	bool result = true;
	cout << endl << "Creating Matrix of size 4X4";
	Matrix test(2, 3);
	cout << endl << "Created Matrix A's rows = " << test.rows << "\tMatrix A's cols = " << test.cols;
	test.show();

	cout << endl << "Creating Matrix of size 4X4";
	Matrix test2(3, 2);
	cout << endl << "Created Matrix B's rows = " << test2.rows << "\tMatrix B's cols = " << test2.cols;
	test2.show();

	time_t t_set;
	time_t t_end;
	time(&t_set);
	auto_ptr<Matrix> R1(test * test2);
	time(&t_end);
	cout << endl << "Running iterative solution, Time Taken: " << difftime(t_end, t_set) << "s" << endl;
	time(&t_set);
	auto_ptr<Matrix> R2(test.strassen(&test2));
	time(&t_end);
	cout << endl << "Running Strassen solution, Time Taken: " << difftime(t_end, t_set) << "s" << endl;

	cout << endl << "A * B" << endl;

	R1->show();
	cout << endl;
	R2->show();

	result &= (*R1 == *R2);

	cout << endl << endl;

	return result;
}